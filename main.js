// a.
let tinggi = parseInt(prompt("masukkan nilai tinggi: "));
let alas = parseInt(prompt("masukkan nilai alas: "));

let luas = tinggi * alas / 2;

console.log(luas);

// b.
const PI = 3.14;

let r = parseInt(prompt("masukkan nilai jari-jari: "));
let luas = PI * r * r;
console.log(luas);

// c.
let profile = {
    id: "00001",
    namaDepan: "Raihan",
    namaBelakang: "Luthfi",
    usia: 24,
    isMarried: false,
};
    
console.log(profile.id); // 00001
console.log(profile.namaDepan); // Raihan
console.log(profile.namaBelakang); // Luthfi
console.log(profile.usia); // 24
console.log(profile.isMarried); // false

// d.
let cities = [
    "Jakarta",
    "Bandung",
    "Surabaya",
    "Medan",
    "Makassar"
];
    
cities.forEach((city) => { console.log(city); });
    
    
